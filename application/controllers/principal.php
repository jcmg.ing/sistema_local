<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal extends CI_Controller { 

	
	public function __construct()
    {

        parent::__construct();
        $this->load->database();
        $this->load->library(['ion_auth', 'form_validation']);
        $this->load->helper(['url', 'language']);

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth'); 
        $this->layout->setLayout('template1'); 
    }
	
	public function index()
	{ 
        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        } 


        $this->layout->view('index');   
	}

    public function buscador()
    { 
        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        } 
        if($this->input->post())
        {
            $buscar=$this->input->post("buscar",true);
             
            $where = "acta_recepcion.nro_acta = acta_peritacion.nro_acta AND
  acta_peritacion.nro_acta = descarte.nro_acta AND
  descarte.nro_acta = barrido.nro_acta";


            $query=$this->db
            ->select("*")
            ->from("div_inf_for.acta_recepcion,quimica.barrido,quimica.acta_peritacion, quimica.descarte")
            ->where($where)
            ->like('acta_recepcion.nro_acta',$buscar)
            ->or_like('acta_peritacion.nro_acta',$buscar)
            ->or_like('descarte.nro_acta',$buscar)
            ->get();
            $query->result();
 
        }
        $this->layout->view('buscador',compact($query));   
    }

}
