<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>SLCCT-GNB</title>
        <meta name="author" content="DevOOPS">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="<?php echo base_url().'public/img/icon2.png'?>" />
 
         <!--*************diseño AdminLTE para el sistema*****************-->
         <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url()?>public/AdminLTE/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url()?>public/AdminLTE/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="<?php echo base_url()?>public/AdminLTE/dist/css/skins/skin-red.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()?>public/AdminLTE/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />
     <!--*************fin diseño AdminLTE para el sistema*****************-->
 <body class="login-page">    

<div class="login-box">
      <div class="login-logo">
        <a href="#"><b>Admin</b>-SLCCT-GNB</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">SISTEMA DE LABORATORIOS CRIMINALÍSTICOS, CIENTÍFICOS Y TECNOLÓGICOS DE LA GUARDIA NACIONAL BOLIVARIANA</p>

        <form   method="post" action="<?php echo base_url()?>principal"> 
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Usuario" name="campousuario" value="" />
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div> 
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Contraseña" name="campoclave" value="" />
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row"> 
            <div class="col-xs-12"> 
                <a class="btn btn-primary btn-block btn-flat" href="principal">Entrar</a>
               
            </div><!-- /.col -->
          </div>
        </form>
 

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
 </body>
</html>