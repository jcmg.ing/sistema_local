<?php $this->load->view('layouts/_header') ?>
<?php $this->load->view('layouts/_menu_principal') ?>



<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            DEPARTAMENTO DE QUÍMICA
            <small>DEMO</small>
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Your Page Content Here -->
			<div class="box">
            	<div class="box-header">
                	<h3 class="box-title">ELABORACION DE ACTAS 2020 <a class="btn btn-danger btn-xs">NUEVO</a> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                	<!-- ACTAS DE PERITACION --> 
                <div class="col-lg-3 col-xs-6">
	              <!-- small box -->
	              <div class="small-box bg-aqua">
	                <div class="inner">
	                  <h2>PERITACIÓN 2020</h2> 
	                </div>
	                <a href="<?php echo base_url()?>quimica/peritacion/2020" >
	                <div class="icon">
	                  <i class="fa  fa-file-word-o"></i>
	                </div>
	                <a href="<?php echo base_url()?>quimica/peritacion/2020" class="small-box-footer">Mas Información <i class="fa fa-arrow-circle-right"></i></a>
	            </a>
	              </div>
	            </div><!-- ./col -->  

	            <!-- ACTAS DE BARRIDO --> 
                <div class="col-lg-3 col-xs-6">
	              <!-- small box -->
	              <div class="small-box bg-aqua">
	                <div class="inner">
	                  <h2>BARRIDO 2020</h2> 
	                </div>
	                <a href="<?php echo base_url()?>quimica/barrido/2020" >
	                <div class="icon">
	                  <i class="fa  fa-file-word-o"></i>
	                </div>
	                <a href="<?php echo base_url()?>quimica/barrido/2020" class="small-box-footer">Mas Información <i class="fa fa-arrow-circle-right"></i></a>
	            </a>
	              </div>
	            </div><!-- ./col -->  
 

	            <!-- ACTAS DE DESCARTE --> 
                <div class="col-lg-3 col-xs-6">
	              <!-- small box -->
	              <div class="small-box bg-aqua">
	                <div class="inner">
	                  <h2>DESCARTES 2020</h2> 
	                </div>
	                <a href="<?php echo base_url()?>quimica/descarte/2020" >
	                <div class="icon">
	                  <i class="fa  fa-file-word-o"></i>
	                </div>
	                <a href="<?php echo base_url()?>quimica/descarte/2020" class="small-box-footer">Mas Información <i class="fa fa-arrow-circle-right"></i></a>
	            </a>
	              </div>
	            </div><!-- ./col -->  

	            <!-- ACTAS DE TOMA DE MUESTRA TOXICOLOGICA --> 
                <div class="col-lg-3 col-xs-6">
	              <!-- small box -->
	              <div class="small-box bg-aqua">
	                <div class="inner">
	                  <h2>TOXICOLOGÍA 2020</h2> 
	                </div>
	                <a href="<?php echo base_url()?>quimica/toxicologico/2020" >
	                <div class="icon">
	                  <i class="fa  fa-file-word-o"></i>
	                </div>
	                <a href="<?php echo base_url()?>quimica/toxicologico/2020" class="small-box-footer">Mas Información <i class="fa fa-arrow-circle-right"></i></a>
	            </a>
	              </div>
	            </div><!-- ./col -->  
 
                </div><!-- /.box-body -->
            </div><!-- /.box -->




             <!-- Your Page Content Here -->
			<div class="box">
            	<div class="box-header">
                	<h3 class="box-title">ACTAS 2019</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                	<!-- ACTAS DE PERITACION --> 
                <div class="col-lg-3 col-xs-6">
	              <!-- small box -->
	              <div class="small-box bg-aqua">
	                <div class="inner">
	                  <h2>PERITACIÓN 2019</h2> 
	                </div>
	                <div class="icon">
	                  <i class="fa  fa-file-word-o"></i>
	                </div>
	                <a href="<?php echo base_url()?>quimica/peritacion/2019" class="small-box-footer">Mas Información <i class="fa fa-arrow-circle-right"></i></a>
	              </div>
	            </div><!-- ./col -->  
 
 
                </div><!-- /.box-body -->
            </div><!-- /.box -->

 
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php $this->load->view('layouts/_footer') ?>