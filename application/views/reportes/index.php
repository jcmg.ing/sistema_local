 <?php $this->load->view('layouts/_header') ?>
<?php $this->load->view('layouts/_menu_principal') ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      REPORTES
      <small>DEMO</small>
    </h1>
  </section>


  <!-- Main content -->
  <section class="content"> 
    <!-- Your Page Content Here -->

    <div class="box">

      <div class="box-header with-border"> 
        <h3 class = "box-title"> Reportes </h3> 
        <div class = "box-tools pull-left" > 
          <!-- ¡Aquí se pueden colocar botones, etiquetas y muchas otras cosas!--> 
          <!-- Aquí hay una etiqueta, por ejemplo-->  
        </div> <!-- /.box-tools--> 
      </div><!-- /.box-header -->

      <div class="box-body"> 
        <div class="row">
          <div class="col-lg-3 col-xs-6">
            <!-- small box --> 
            <div class="small-box bg-aqua">
              <div class="inner">
                <h3>DICTAMENES</h3>
                <p>ENTREGADOS</p>
              </div>
              <a href="<?php echo base_url()?>secretaria/dictamenes_entregados/" >
                <div class="icon">
                  <i class="fa  fa-file-word-o"></i>
                </div> 
                <a href="<?php echo base_url()?>secretaria/dictamenes_entregados/" class="small-box-footer"> Mas Informacion <i class="fa fa-arrow-circle-right"></i></a>
              </a>
            </div>
          </div><!-- ./col -->    



          <!-- ACTAS DE RECEPCION --> 
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->    
            <div class="small-box bg-aqua">        
              <div class="inner">
                <h3>RECEPCIÓN</h3>
                <p> DE LA SOLICITUD</p>
              </div>
              <a href="<?php echo base_url()?>secretaria/remision" >
                <div class="icon">
                  <i class="fa  fa-file-word-o"></i>
                </div> 
                <a href="<?php echo base_url()?>secretaria/remision" class="small-box-footer"> Mas Informacion <i class="fa fa-arrow-circle-right"></i></a>
              </a>    
            </div>
          </div><!-- ./col -->
        </div>  <!-- ./fin row -->
    </div> 
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

 